﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FindMiAdmin
{
    class Program
    {
        static List<string> visitableAddress = new List<string>();

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            #region  initUrlQueue
            Queue<string> urlQueue = new Queue<string>();
            int highBit = 0;
            int lowBit = 0;
            for (highBit = 0; highBit < 256; highBit++)
            {
                for (lowBit = 0; lowBit < 256; lowBit++)
                {
                    urlQueue.Enqueue($"http://192.168.{highBit}.{lowBit}");
                }
            }
            #endregion

            #region threadMethod
            ThreadStart requestThreadStart = new ThreadStart(() =>
             {
                 while (true)
                 {

                     var url = urlQueue.Dequeue();
                     Console.WriteLine("trying " + url);
                     var rs = WebHelper.Get(url);
                     //var rs = WebHelper.GetAsync(url).Result;
                     if (!string.IsNullOrEmpty(rs))
                     {
                         lock (visitableAddress)
                         {
                             visitableAddress.Add(url);
                         }
                         Console.WriteLine("nice !!!! got admin " + url);
                     }
                     Thread.Sleep(1000);
                 }

             });
            #endregion



            Thread[] threadArray = new Thread[10];//8
            for (int i = 0; i < threadArray.Length; i++)
            {
                threadArray[i] = new Thread(requestThreadStart);
                threadArray[i].Start();
                Thread.Sleep(100);
            }




            var count = 1;
            while (true) //
            {
                // Console.WriteLine("Report Time:" + DateTime.Now + "---------------------" + count);
                // Console.WriteLine("checking status: program running normally");
                count += 1;
                Thread.Sleep(1000);
                if (count % 5 == 0)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("-------------result set---------------");
                    foreach (var addr in visitableAddress)
                    {
                        Console.WriteLine("address:" + addr);
                    }
                    Console.WriteLine("-------------------------------------");
                    Console.WriteLine("\n");
                }
            }
        }

        static void TestUrl(string url)
        {
            var rs = WebHelper.Get(url);
            //var rs = WebHelper.GetAsync(url).Result;
            if (!string.IsNullOrEmpty(rs))
            {
                lock (visitableAddress)
                {
                    // adminUrl = url;
                    visitableAddress.Add(url);
                }

                Console.WriteLine("nice !!!! got admin " + url);
            }

        }

    }
}
