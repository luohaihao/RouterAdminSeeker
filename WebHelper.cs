
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FindMiAdmin
{

 public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            //w.Timeout = 20 * 60 * 1000;
            w.Timeout = 5 * 1000;
            return w;
        }
    }

    public class WebHelper
    {
        /// <summary>
        /// 简单的HTTP GET方法
        /// </summary>
        /// <param name="url"></param>
        /// <returns>response</returns>
        public static string Get(string url,string pageEncoding="")
        {
            try
            {

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Timeout = 5000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
               // StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                // Open the stream using a StreamReader for easy access.
                Encoding encoding;
                switch (pageEncoding.ToLower())
                {
                    case "utf-8":
                        encoding = Encoding.UTF8;
                        break;
                    case "unicode":
                        encoding = Encoding.Unicode;
                        break;
                    case "ascii":
                        encoding = Encoding.ASCII;
                        break;
                    default:
                        encoding = Encoding.Default;
                        break;

                }

                StreamReader reader = new StreamReader(dataStream, encoding);

                string responseFromServer = reader.ReadToEnd();

                // Cleanup the streams and the response.
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
                return "";
            }

        }

        /// <summary>
        /// 简单的HTTP GET异步方法
        /// </summary>
        /// <param name="url"></param>
        /// <returns>response</returns>
        public static async Task<string> GetAsync(string url)
        {
            try
            {
                var client = new MyWebClient();
                var response = await client.DownloadStringTaskAsync(url);
                return response;
                //static HttpClient client = new HttpClient();
                //HttpResponseMessage response = await client.GetAsync(path);
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
                return "";
            }

        }

    }
}